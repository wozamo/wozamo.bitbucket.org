/* intro section centered */
(function () {
    /* intro window height */
    var imageFit = function () {
        var windowHeight = $(window).height();
        $("#title").css("min-height", windowHeight);
    };

    /* center intro content vertically */
    var center = function () {
        var windowHeight = $(window).height(),
            navHeight = $("nav").height();
        $(".title-wrapper").css("padding-top", (windowHeight / 2) - (navHeight / 2));
    };

    imageFit();
    center();

    $(window).resize(function () {
        imageFit();
        center();
    });
})();

/* preloader */
(function () {
    $(window).load(function () {
        $('#status').fadeOut();
        $('#preloader').delay(350).fadeOut('slow');
        $('body').css({'overflow': 'visible'});
    });
})();

/* intro slider */
(function () {
    var slider = $(".intro-slider"),
        slide = $(".intro-slider span"),
        height = slide.height(),
        numberDivs = slider.children().length,
        first = slide.eq(0),
        current = 1;


    setInterval(function () {
        var number = current * -height;
        if (current === numberDivs) {
            first.animate({
                'margin-top': "0"
            }, 700);
            current = 1;
        } else {
            number = current * -height;

            first.animate({
                'margin-top': number
            }, 700, function () {
                current++;
            });
        }
    }, 2500);
})();

/* social icons bounce */
(function () {
    $.easing['jswing'] = $.easing['swing'];
    $.extend($.easing, {
        easeOutBounce: function (x, t, b, c, d) {
            if ((t /= d) < (1 / 2.75)) {
                return c * (7.5625 * t * t) + b;
            } else if (t < (2 / 2.75)) {
                return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
            } else if (t < (2.5 / 2.75)) {
                return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
            } else {
                return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
            }
        }
    });

    $(".gHover").hover(function () {
        $(this).stop().animate({
            top: -10
        }, 400, "easeOutBounce");
    }, function () {
        $(this).stop().animate({
            top: 0
        }, 900, "easeOutBounce");
    });
})();

/* nav responsive slide down */
(function () {
    var current_width = $(window).width(),
        menu = $(".menu"),
        menuBtn = $(".menu-btn");

    var hideMenu = function () {
        if (current_width < 768) {
            menuBtn.removeClass("is-hidden");
            menu.addClass("is-hidden").css("display", "none");
        } else {
            menu.addClass("is-displayed")
                .css("display", "block");
            menuBtn.addClass("is-hidden");
        }
    };

    $(window).resize(function () {
        current_width = $(window).width();
        hideMenu();
    });

    menuBtn.on("click", function () {
        menu.slideToggle();
    });

    hideMenu();
})();

/* projects slide up in about section */
(function () {
    $('.animated').appear();

    $(document.body).on("appear", ".slide", function () {
        $(this).each(function () {
            $(this).addClass("ae-animation-slide");
        });
    });
})();

/* parallax */
(function () {
    function is_touch_device() {
        return "ontouchstart" in window // works on most browsers
            || "onmsgesturechange" in window; // works on ie10
    };

    $("body").localScroll(600);

    if (!(is_touch_device())) {
        //.parallax(xPosition, speedFactor, outerHeight) options:
        //xPosition - Horizontal position of the element
        //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
        //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
        $("#title").parallax("50%", -0.2);
//        $("#about").parallax("100%", 0.9);
        $("#portfolio").parallax("50%", 0.3, true);
        $("#footer").parallax("50%", -0.4);
        $(".footer-container").parallax("50%", 0.2);
    }
})();

